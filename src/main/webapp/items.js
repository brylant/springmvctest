var smvct = smvct || {};

smvct.ItemsController = function (storageController) {

    this.storeItems = function () {
        if (storageController.isEnabled()) {
            var itemsJson = JSON.stringify(readItemsFromInputs());
            localStorage.setItem("smvct.items", itemsJson);
            console.log("stored items: " + itemsJson);
        }
    };

    this.restoreItems = function () {
        if (storageController.isEnabled()) {
            var items = readItemsFromLocalStorage();
            for (var i = 0; i < items.length; i++) {
                $(".items-tbl tbody").append(createRowHtml(items[i]));
            }
            $(".items-tbl tbody td:last").append(createActBtnsHtml());
            console.log("restored items: " + JSON.stringify(items));
        }
    };

    this.onAddBtnClick = function () {
        var actionBtnCnt = $(".action-btn-cnt").detach();
        $(".items-tbl tbody tr:last").after(createRowHtml(new smvct.Item("", "", "")));
        $(".items-tbl tbody td:last").append(actionBtnCnt);
    };

    this.onRemBtnClick = function () {
        if (itemsInTable() > 1) {
            var actionBtnCnt = $(".action-btn-cnt").detach();
            $(".items-tbl tbody tr:last").remove();
            $(".items-tbl tbody td:last").append(actionBtnCnt);
        }
    };

    function itemsInTable() {
        return $(".items-tbl tbody tr").length;
    }

    function createInputHtml(modelIndex, fieldName, text) {
        return "<input type='text' name='items[" + modelIndex + "]." + fieldName + "' value='" + text + "'/>";
    }

    function createActBtnsHtml() {
        var btnRemHtml = "<button class='action-btn rem'>-</button>";
        var btnAddHtml = "<button class='action-btn add'>+</button>";
        return "<div class='action-btn-cnt'>" + btnRemHtml + " " + btnAddHtml + "</div>";
    }

    function createRowHtml(item) {
        var modelIndex = itemsInTable();
        var cell1Html = "<td>" + createInputHtml(modelIndex, "col1", item.col1) + "</td>";
        var cell2Html = "<td>" + createInputHtml(modelIndex, "col2", item.col2) + "</td>";
        var cell3Html = "<td>" + createInputHtml(modelIndex, "col3", item.col3) + "</td>";
        var cell4Html = "<td></td>";
        return "<tr>" + cell1Html + cell2Html + cell3Html + cell4Html + "</tr>";
    }

    function readItemsFromInputs() {
        var items = [];
        $(".items-tbl tbody tr").each(function (modelIndex, row) {
            var inputs = $(row).find("input").get();
            var item = new smvct.Item($(inputs[0]).val(), $(inputs[1]).val(), $(inputs[2]).val());
            items.push(item);
        });
        return items;
    }

    function readItemsFromLocalStorage() {
        var itemsToCreate = [];
        var itemsJson = localStorage.getItem("smvct.items");
        if (itemsJson) {
            itemsToCreate = JSON.parse(itemsJson);
        } else {
            itemsToCreate = [ new smvct.Item("", "", "") ];
        }
        return itemsToCreate;
    }
};

smvct.Item = function (col1, col2, col3) {
    this.col1 = col1;
    this.col2 = col2;
    this.col3 = col3;
};

smvct.StorageController = function () {

    var storageEnabled = true;

    this.enable = function () {
        storageEnabled = true;
    };

    this.disable = function () {
        storageEnabled = false;
    };

    this.isEnabled = function () {
        return storageEnabled;
    };

    this.clear = function() {
        localStorage.clear();
    };
};

$(function () {
    var storageController = new smvct.StorageController();
    $(".action-btn.storage").click(function () {
        if (storageController.isEnabled()) {
            storageController.disable();
            $(this).text("Storage disabled");
        } else {
            storageController.enable();
            $(this).text("Storage enabled");
        }
    });
    $(".action-btn.clr-storage").click(function() {
        storageController.clear();
    });

    var itemsController = new smvct.ItemsController(storageController);
    itemsController.restoreItems();
    $(".action-btn.add").on("click", itemsController.onAddBtnClick);
    $(".action-btn.rem").on("click", itemsController.onRemBtnClick);
    $(window).unload(itemsController.storeItems);
});
